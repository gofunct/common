module github.com/gofunct/common

require (
	contrib.go.opencensus.io/exporter/stackdriver v0.6.0
	github.com/Masterminds/sprig v2.16.0+incompatible
	github.com/aws/aws-sdk-go v1.16.11
	github.com/bradleyjkemp/cupaloy v2.3.0+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gofunct/bingen v0.0.0-20190102020125-13eef7615a05 // indirect
	github.com/gofunct/gogen v0.1.1
	github.com/golang/mock v1.2.0
	github.com/golang/protobuf v1.2.0
	github.com/google/go-cmp v0.2.0
	github.com/google/wire v0.2.0
	github.com/gorilla/mux v1.6.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/grpc-gateway v1.6.3
	github.com/haya14busa/reviewdog v0.0.0-20180723114510-ffb00ef78fd3
	github.com/iancoleman/strcase v0.0.0-20180726023541-3605ed457bf7
	github.com/izumin5210/grapi v0.3.2
	github.com/jessevdk/go-assets-builder v0.0.0-20130903091706-b8483521738f
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a
	github.com/kisielk/errcheck v1.1.0
	github.com/kyokomi/emoji v2.0.0+incompatible
	github.com/mattn/go-colorable v0.0.9
	github.com/mitchellh/gox v0.4.0
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/piotrkowalczuk/promgrpc v2.0.2+incompatible // indirect
	github.com/pkg/errors v0.8.0
	github.com/serenize/snaker v0.0.0-20171204205717-a683aaf2d516
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/soheilhy/cmux v0.1.4
	github.com/spf13/afero v1.2.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/srvc/wraperr v0.2.0
	go.opencensus.io v0.18.0
	go.uber.org/zap v1.9.1
	gocloud.dev v0.8.0
	golang.org/x/lint v0.0.0-20181217174547-8f45f776aaf1
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4
	google.golang.org/api v0.0.0-20181017004218-3f6e8463aa1d
	google.golang.org/genproto v0.0.0-20181221175505-bd9b4fb69e2f
	google.golang.org/grpc v1.17.0
	gopkg.in/dixonwille/wlog.v2 v2.0.0
	gopkg.in/dixonwille/wmenu.v4 v4.0.2 // indirect
	honnef.co/go/tools v0.0.0-20190102075043-fe93b0e3b36b
	k8s.io/utils v0.0.0-20181221173059-8a16e7dd8fb6
	mvdan.cc/unparam v0.0.0-20181201214637-68701730a1d7
)
